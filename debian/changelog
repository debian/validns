validns (0.8+git20230810.a7e6b4f-2) unstable; urgency=medium

  * Orphaning this package per request of Casper in Feb 2025, on Meuknet IRC:
    уто 04 16:28 <casper_> laat maar vallen.
    - d/control: changing Maintainer: Casper Gielen <casper-alioth@gielen.name>,
      Uploaders: Joost van Baal-Ilić <joostvb@debian.org> to Maintainer: Debian
      QA Group <packages@qa.debian.org>.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Fri, 07 Feb 2025 17:09:14 +0100

validns (0.8+git20230810.a7e6b4f-1) unstable; urgency=medium

  * Team upload

  [ Jelmer Vernooĳ ]
  * Migrate repository from alioth to salsa.
    Closes: #935011

  [ Andreas Tille ]
  * Latest upstream commit
    Closes: #935012, #935013
  * Point watch file to Github
  * d/rules: Its no example
  * Homepage is now Github
  * Standards-Version: 4.7.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Secure URI in copyright format (routine-update)
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Remove trailing whitespace in debian/copyright (routine-update)
  * Remove trailing whitespace in debian/rules (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Trim trailing whitespace.
  * Fix day-of-week for changelog entry 0.6-1.

 -- Andreas Tille <tille@debian.org>  Thu, 26 Dec 2024 15:25:07 +0100

validns (0.8+git20160720-3.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Get it compiled against OpenSSL 3.0+ (Closes: #1006583).

 -- Sebastian Andrzej Siewior <sebastian@breakpoint.cc>  Thu, 23 Jun 2022 18:48:15 +0200

validns (0.8+git20160720-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Avoid a warning regarding string truncation (Closes: #897882).
  * Get it compiled against OpenSSL 1.1+ (Closes: #859784).
  * Use priority optional instead of extra.

 -- Sebastian Andrzej Siewior <sebastian@breakpoint.cc>  Fri, 22 Feb 2019 23:52:58 +0100

validns (0.8+git20160720-3) unstable; urgency=medium

  * debian/copyright Add License: statement.
  * debian/control Upgrade to Standards-Version 3.9.8 (no changes).

 -- Casper Gielen <casper-alioth@gielen.name>  Wed, 14 Dec 2016 16:01:55 +0100

validns (0.8+git20160720-2) unstable; urgency=medium

  * Depend on OpenSSL 1.0 (Closes: #828591)

 -- Casper Gielen <casper-alioth@gielen.name>  Thu, 01 Dec 2016 14:51:30 +0100

validns (0.8+git20160720-1) stable; urgency=medium

  * New upstream with support for INCLUDE.

 -- Casper Gielen <casper-alioth@gielen.name>  Wed, 20 Jul 2016 11:27:36 +0200

validns (0.8-2) unstable; urgency=low

  * debian/control Point Vcs-Git & Vcs-Browser to the repo with the packaging.
  * debian/patches/fix-dont-overwrite-cflagspatch Improve description.
  * debian/docs/changes Remove upstream changelog to avoid duplicate inclusion.

 -- Casper Gielen <casper-alioth@gielen.name>  Fri, 08 Aug 2014 16:09:46 +0200

validns (0.8-1) unstable; urgency=low

  * New upstream.
  * Support ECDSA and SHA-256 in SSHFP.
  * Unapply debian patches that were incorporated by mistake.
  * debian/control Standards version 3.9.5 (no changes)

 -- Casper Gielen <casper-alioth@gielen.name>  Fri, 04 Apr 2014 16:09:46 +0200

validns (0.7-2) unstable; urgency=low

  * debian/control Remove leading space from description.
  * debian/copyright Change Format URL.
  * Remove base32hex-test binary from source and fix Makefile clean target.

 -- Casper Gielen <casper-alioth@gielen.name>  Sat, 29 Jun 2013 22:57:24 +0200

validns (0.7-1) unstable; urgency=low

  [ Casper Gielen ]
  * New upstream.
  * Use watch file.
  * Add Hardening to Makefile so it is actually used.
  * Remove lintian-override for new-package-should-close-itp-bug.

  [ Joost van Baal-Ilić ]
  * First upload to Debian. (Closes: #702507)

 -- Casper Gielen <casper-alioth@gielen.name>  Tue, 16 Apr 2013 13:55:58 +0200

validns (0.6r250-2) unstable; urgency=low

  * Include hardening.
  * BuildDepend on dpkg-dev.
  * Switch to debhelper v9 compatibility.
  * Fix maintainer.

 -- Casper Gielen <casper-alioth@gielen.name>  Tue, 16 Apr 2013 13:35:21 +0200

validns (0.6r250-1) unstable; urgency=low

  * New upstream
  * Fix uploader in changelog

 -- Casper Gielen <casper-alioth@gielen.name>  Tue, 16 Apr 2013 10:26:58 +0200

validns (0.6r239-2) unstable; urgency=low

  * Converted to git-buildpackage

 -- Casper Gielen <casper-alioth@gielen.name>  Fri, 12 Apr 2013 15:12:12 +0200

validns (0.6r239-1) unstable; urgency=low

  [ Casper Gielen ]
  * New upstream

  [ Joost van Baal-Ilić ]
  * debian/control: add myself to Uploaders

 -- Casper Gielen <casper-alioth@gielen.name>  Fri, 08 Mar 2013 13:13:25 +0100

validns (0.6r236-2) unstable; urgency=low

  * Unit tests weer aangezet

 -- Casper Gielen <casper-alioth@gielen.name>  Tue, 08 Jan 2013 13:03:55 +0100

validns (0.6r236-1) unstable; urgency=low

  * Checks weer aanzetten.

 -- Casper Gielen <casper-alioth@gielen.name>  Tue, 08 Jan 2013 13:03:16 +0100

validns (0.6-2) unstable; urgency=low

  * Actually do include binary
  * Fixed dependencies
  * debian/lintian-overrides: added. possible-gpl-code-linked-with-openssl:
    upstream code is released under modified BSD license.

 -- Casper Gielen <casper-alioth@gielen.name>  Tue, 08 Jan 2013 12:54:16 +0100

validns (0.6-1) unstable; urgency=low

  * Initial release.

 -- Casper Gielen <casper-alioth@gielen.name>  Fri, 04 Jan 2013 17:57:57 +0200
